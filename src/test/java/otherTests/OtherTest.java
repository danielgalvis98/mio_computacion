package otherTests;

import static org.testng.Assert.assertTrue;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import com.javaoutofbounds.pojo.TestCont;

public class OtherTest {
	
	@Autowired
	TestCont cont;
	
	@Test
	void esOtro() {
		cont = new TestCont();
		assertTrue(cont.testBool());
	}

}
