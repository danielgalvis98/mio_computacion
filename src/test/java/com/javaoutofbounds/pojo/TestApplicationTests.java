package com.javaoutofbounds.pojo;


import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;

@SpringBootTest
class TestApplicationTests {

	@Autowired
	TestCont cont;
	
	@Test
	void contextLoads() {
		cont = new TestCont();
		assertTrue(cont.testBool());
	}
	
	

}
